#include <stdint.h>

uint64_t newtons_inverse(uint64_t n)
{
    uint64_t a = (n&1)?n:n+1;
    uint64_t x0 = (3*a)^2;
    uint64_t x1 = x0*(2 - a*x0);
    uint64_t x2 = x1*(2 - a*x1);
    uint64_t x3 = x2*(2 - a*x2);
    uint64_t x4 = x3*(2 - a*x3);
    return x4;
}

#include <stdio.h>

int main(){
    uint64_t n;
    for(n=35;n<120;n++){
        uint64_t i=newtons_inverse(n);
        uint64_t p = (i*n);
        printf("%llu * %llu = %llu\n",n,i,p);
    }
    return 0;
}
